# GoShimmer testing network

This project aims to help testing a network of IOTA
[goshimmer](https://github.com/iotaledger/goshimmer).
For the moment it relies on `docker` (based on goshimmer
`tools/docker-network`), but support to another backends (SSH) is to come.

## Docker-based network

Currently auto-iota-network makes it possible to run a network of goshimmer on
docker. You can configure different the nodes via the `list_of_nodes.yml` file.
An example file like the one below is available in the root directory:

```yml
{
"alice":{
	"transactions_per_second":1,
	"POW_difficulty":1
    },
"bob":{
	"transactions_per_second":1,
	"POW_difficulty":11
    }
}
```

## Prerequisites

Other than `docker`, this tool relies on `docker-compose` and `git`. On a
Debian machine, you can install them with:

`apt install docker.io docker-compose git`

## Basic use

For being able to run the docker network (and each time the `goshimmer`
submodule needs to be updated), you need to build first the goshimmer docker
image:

```
git submodule update --init --recursive --remote
./build_GoShimmer.sh
```

Then, you can create the `list_of_nodes.yml` file, adding as many goshimmer
nodes as you want (and supported by your machine).
Take into account that the current configuration limits the use of two CPU
cores per node.

Finally, run the network with:

`./run.sh`

Once you want to stop the network, you can turn it off with:

`./down.sh`
