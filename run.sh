#!/bin/sh

# Create config folders and wallet directories
echo "Creating docker-compose.yml..."
python3 scripts/create_docker_compose_file.py
echo "Creating prometheus.yml..."
python3 scripts/create_prometheus_yml.py
echo "Creating wallet directories..."
echo "Removing old wallets..."
rm -r wallets/*
echo "Creating directories..."
python3 scripts/create_wallets.py

echo "Launching iota network..."
docker-compose up -d

# TODO: handle spam

echo "Waiting five seconds before launching transactions"
sleep 5

echo "Launching transaction issuers..."
./scripts/launch_transaction_issuers.sh

echo ""
echo "DONE : $(date -u "+%Y-%m-%dT%H:%M:%S") (UTC)"
echo ""
