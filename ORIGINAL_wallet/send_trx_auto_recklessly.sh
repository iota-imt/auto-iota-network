#!/bin/sh

set -x

if [ "$1" = "" ] ;
then
    echo "Utilisation : ./send_trx_auto_recklessly.sh [duration between two transactions]"
    exit 1
fi

WAIT_TIME=$1
DEST_ADDRESS=$(../../rand-address)
OUTPUT_CSV_FILE="trx_duration.csv"
BALANCE_COMMAND="./cli-wallet_Linux_x86_64 balance"
GET_BALANCE_INT="python3 ../../scripts/get_balance_int.py"

while :
do
    ./cli-wallet_Linux_x86_64 balance
    BALANCE_COMMAND_RESULT=$?
    if [ "$BALANCE_COMMAND_RESULT" -ne 0 ] ;
    then
        echo "NODE'S API IS NOT REACHABLE, STOPPING AUTOMATIC TRANSACTIONS ..."
        exit 1
    fi
    t_start=$(date +%s%3N)
    # Send 1 iota
    ./cli-wallet_Linux_x86_64 send-funds -amount 1 -dest-addr $DEST_ADDRESS
    t_end=$(date +%s%3N)
    duration=$(($t_end-$t_start))
    echo "$t_end,$duration,$($GET_BALANCE_INT)" >> $OUTPUT_CSV_FILE
    sleep $WAIT_TIME
done
