#!/usr/bin/env python3

# TPS = transaction per second

import os, json
from create_docker_compose_file import replicate_nodes

TPS="transactions_per_second"
LIST_OF_NODES_FILE="list_of_nodes.json"
PORT_DICT_FILE="port_dict.json"
WALLET_DIR="wallets"
ORIGINAL_WALLET="ORIGINAL_wallet"

def create_wallets(list_of_nodes_file=LIST_OF_NODES_FILE,
        wallet_dir=WALLET_DIR, original_wallet=ORIGINAL_WALLET,
        port_dict_file=PORT_DICT_FILE):
    with open(list_of_nodes_file, 'r') as f : nodes=json.load(f)
    keep_nodes_with_tps(nodes)
    replicate_nodes(nodes)
    create_wallet_directories(nodes, wallet_dir,
            port_dict_file, original_wallet)

def keep_nodes_with_tps(nodes):
    for node_name in list(nodes.keys()):
        if TPS not in nodes[node_name] or nodes[node_name][TPS]<=0:
            del(nodes[node_name])

def create_wallet_directories(nodes, wallet_dir_path,
        port_dict_file, original_wallet_path):
    if wallet_dir_path[-1]!="/":
        wallet_dir_path+="/"
    if original_wallet_path[-1]!="/":
        original_wallet_path+="/"

    if not os.path.isdir(wallet_dir_path) : # If "wallets/" does not exist
        os.mkdir(wallet_dir_path)

    port_dict, send_trx_auto_code, send_trx_auto_code, send_trx_auto_recklessly_code =\
        read_files_for_wallet_dir_creation(port_dict_file, original_wallet_path)

    for node in nodes:
        create_node_wallet(node, wallet_dir_path, original_wallet_path,
        port_dict, nodes, send_trx_auto_code, send_trx_auto_recklessly_code)

# return port_dict, send_trx_auto_code, send_trx_auto_code
def read_files_for_wallet_dir_creation(port_dict_file, original_wallet_path):
    with open(port_dict_file, 'r') as f:
        port_dict=json.load(f)
    with open(original_wallet_path+"send_trx_auto.sh", 'r') as f:
        send_trx_auto_code=f.read()
    with open(original_wallet_path+"send_trx_auto_recklessly.sh", 'r') as f:
        send_trx_auto_recklessly_code=f.read()
    return port_dict, send_trx_auto_code, \
        send_trx_auto_code, send_trx_auto_recklessly_code

# Does not intialize the wallet
def create_node_wallet(node, wallet_dir_path, original_wallet_path,
        port_dict, nodes, send_trx_auto_code, send_trx_auto_recklessly_code):
    new_dir_path=wallet_dir_path+node+"/"
    os.system("cp -r "+original_wallet_path+" "+new_dir_path)
    conf_dict={"WebAPI": "http://127.0.0.1:"+str(port_dict[node]["API"])}
    with open(new_dir_path+"config.json", 'w') as f:
        json.dump(conf_dict, f, indent=4)
    wait_time = str(1/nodes[node][TPS])
    with open(new_dir_path+"send_trx_auto.sh", 'w') as f:
        f.write(
            send_trx_auto_code.replace("$1", wait_time)
        )
    with open(new_dir_path+"send_trx_auto_recklessly.sh", 'w') as f:
        f.write(
            send_trx_auto_recklessly_code.replace("$1", wait_time)
        )

if __name__=="__main__":
	create_wallets()
