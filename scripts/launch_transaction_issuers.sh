#!/bin/sh
cd wallets

echo "Initializing wallets and requesting funds..."
echo "Start : $(date -u "+%Y-%m-%dT%H:%M:%S") (UTC)"

for node_name in $(ls)
do
    cd $node_name
    echo "Init wallet for $node_name"
    ./cli-wallet_Linux_x86_64 init
    echo "Requesting funds for $node_name..."
    ./cli-wallet_Linux_x86_64 request-funds
    cd ..
done

echo "Done : $(date -u "+%Y-%m-%dT%H:%M:%S") (UTC)"

sleep 3

echo "Launching transaction issuers ..."
for node_name in $(ls)
do
    cd $node_name
    # launch the transaction issuer in background and send its output in /dev/null
    ./send_trx_auto_recklessly.sh > /dev/null &
    cd ..
done
