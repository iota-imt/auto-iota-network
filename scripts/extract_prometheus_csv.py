#!/usr/bin/env python3

# URL format
# http://tangle-01.enstb.org:9090/api/v1/query_range?query=process_cpu_usage&start=2020-07-15T15:25:00.000Z&end=2020-07-15T15:30:00.000Z&step=5s

import requests
import csv

def main():
    start_date, end_date, step, address, file_name = ask_parameters()
    print("EXTRACTING DATA FROM API...")
    data = extract_dict_of_dicts(start_date, end_date, step, address)
    print("CONVERTING DATA TO MATRIX...")
    delete_global_parameters(data)
    data=convert_to_matrix(data)
    print("WRITING CSV FILE...")
    save_data_as_csv(data, file_name)

def ask_parameters():
    print("(Date format : '2020-07-15T15:25:00', Timezone : UTC)")
    start_date=input("Start date : ")
    end_date=input("End date : ")
    step=input("Step ('5s' by default) : ")
    if step=="":
        step="5s"
    address=input("Node address ('127.0.0.1' by default): ")
    address=parse_node_address(address)
    file_name=input("File name ('prometheus.csv' by default) : ")
    file_name=parse_output_file_name(file_name)
    return start_date, end_date, step, address, file_name

# Return a dictionary of dictionaries
# { "[node]": {
#   "[parameter]": [[time, value], [time, value], [time, value], etc.]
#   }
# }
def extract_dict_of_dicts(start_date, end_date, step, address):
    dict_of_dicts=dict()
    for parameter in get_list_of_parameters(address):
        r = request_to_API(start_date, end_date, step, address, parameter)
        if r["status"]=="error":
            print("\nERROR while requesting "+parameter+"("+r["errorType"]+") : "+r["error"])
        else :
            nbr_of_instances=len(r["data"]["result"])
            for instance in range(nbr_of_instances) :
                add_data(r, dict_of_dicts, instance)
    return dict_of_dicts

def request_to_API(start_date, end_date, step, address, parameter):
    url="http://"+address+\
            ":9090/api/v1/query_range?query="+parameter+\
            "&start="+start_date+"Z&end="+end_date+"Z&step="+step
    return requests.get(url).json()

def add_data(data, dict_of_dicts, instance):
    parameter = data["data"]["result"][instance]["metric"]["__name__"]
    node=data["data"]["result"][instance]["metric"]["instance"].split(":")[0]
    values=data["data"]["result"][instance]["values"]
    if node not in dict_of_dicts :
        dict_of_dicts[node]=dict()
    dict_of_dicts[node][parameter]=values

def delete_global_parameters(data):
    for node in list(data.keys()):
        for parameter in list(data[node].keys()):
            if "global" in parameter:
                del(data[node][parameter])
        if data[node]=={}:
            del(data[node])

# Input : dictionary of dictionaries
# { "[node]": {
#   "[parameter]": [[time, value], [time, value], [time, value], etc.]
#   }
# }
# Output :
# time	node	param_1	param_2 ...
# 1	    alice	10	    3
# 2	    alice	10	    3
# 3	    alice   10	    3
# 1	    bob     20	    5
# 2	    bob	    20	    5
# 3	    bob	    20	    5
#
def convert_to_matrix(data):
    if data=={}:
        return []
    list_of_nodes = list(data.keys())
    list_of_parameters = list(data[list_of_nodes[0]].keys())
    matrix=list()
    matrix.append(["time", "node"]+list_of_parameters)
    time_list = [couple[0] for couple in data[list_of_nodes[0]][list_of_parameters[0]]]
    for node in list_of_nodes:
        for t in range(len(time_list)):
            matrix.append([ time_list[t], node ])  #time & node
            for parameter in data[node] :
                try :
                    matrix[-1].append(data[node][parameter][t][1]) #value
                except :
                    matrix[-1].append("OUT_OF_RANGE")
    return matrix

def save_data_as_csv(data, file_name):
    if len(data)==0:
        print("ERROR : csv file not created : data is empty.")
        return
    with open(file_name, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for row in data:
            writer.writerow(row)

def get_list_of_parameters(address):
    r = requests.get("http://"+address+":9090/api/v1/targets/metadata").json()
    if r["status"]=="error":
        print("\nERROR while requesting metadata ("+r["errorType"]+") : "+r["error"])
        return list()
    else :
        return [metric["metric"] for metric in r["data"]]

def parse_output_file_name(file_name):
    if file_name=="":
        file_name="prometheus.csv"
    elif file_name.split(".")[-1]!="csv":
        file_name+=".csv"
    return file_name

def parse_node_address(node_address):
    if node_address=="":
        return "127.0.0.1"
    node_address=node_address.replace(":9090","")
    node_address=node_address.replace("http://","")
    return node_address

if __name__ == "__main__":
    main()
