#!/usr/bin/env python3

# DC = docker-compose

import json

DC_BEGINNING_FILE="docker-compose-parts/beginning.yml"
DC_NODE_FILE="docker-compose-parts/node.yml"
DC_END_FILE="docker-compose-parts/end.yml"
LIST_OF_NODES_FILE="list_of_nodes.json"
OUTPUT_FILE="docker-compose.yml"
PORT_DICT_FILE="port_dict.json"

def create_docker_compose_file(dc_beginning_file=DC_BEGINNING_FILE,
        dc_node_file=DC_NODE_FILE, dc_end_file=DC_END_FILE,
        list_of_nodes_file=LIST_OF_NODES_FILE, output_file=OUTPUT_FILE,
        port_dict_file=PORT_DICT_FILE):
    dc_beginning, dc_node, list_of_nodes, dc_end = \
            read_files(dc_beginning_file, dc_node_file,
                    list_of_nodes_file, dc_end_file)
    auto_complete(list_of_nodes)
    replicate_nodes(list_of_nodes)
    port_dict=write_file(dc_beginning, dc_node, list_of_nodes, dc_end, output_file)
    with open(port_dict_file, 'w') as f: json.dump(port_dict, f, indent=4)

def read_files(dc_beginning_file, dc_node_file,
        list_of_nodes_file, dc_end_file):
    with open(dc_beginning_file) as f:
        dc_beginning = f.read()
    with open(dc_node_file) as f:
        dc_node = f.read()
    with open(list_of_nodes_file) as f :
        list_of_nodes=json.load(f)
        delete_tps(list_of_nodes)
    with open(dc_end_file) as f:
        dc_end = f.read()
    return dc_beginning, dc_node, list_of_nodes, dc_end

def auto_complete(list_of_nodes):
    for node_name in list_of_nodes:
        node=list_of_nodes[node_name]
        if "POW_difficulty" not in node:
            node["POW_difficulty"]=22
        if "tip_selection" not in node:
            node["tip_selection"]=True
        if "cpus" not in node:
            node["cpus"]=2

def replicate_nodes(list_of_nodes):
    for node_name in list(list_of_nodes.keys()):
        node=list_of_nodes[node_name]
        if "nbr_of_instances" not in node:
            node["nbr_of_instances"]=1
        for i in range(1, node["nbr_of_instances"]+1):
            list_of_nodes[node_name+"_"+str(i)]=node
        del(list_of_nodes[node_name])

def write_file(dc_beginning, dc_node, list_of_nodes, dc_end, output_file_path):
    dc_file = open(output_file_path, 'w')
    dc_file.write(dc_beginning)
    port_dict=write_nodes(dc_file, dc_node, list_of_nodes)
    write_dc_end(dc_file, dc_end, list_of_nodes)
    dc_file.close()
    return port_dict

def delete_tps(list_of_nodes):
    for node_name in list_of_nodes:
        node=list_of_nodes[node_name]
        if "transactions_per_second" in node:
            del(node["transactions_per_second"])

#return port_dict={
# "alice_1":{
#   "API":8080
#   "dashboard":8081
#   }
# "bob_1":{
#   "API":8180
#   "dashboard":8181
#   }
# ...
# }
def write_nodes(dc_file, dc_node, list_of_nodes):
    port_API = 8080
    port_dashboard = 8081
    port_prometheus = 9311
    port_dict=dict()
    cpuset_num = 0
    for node_name in list_of_nodes:
        node = list_of_nodes[node_name]
        dc_node_copy=dc_node
        dc_node_copy=dc_node_copy.replace("[node_name]", node_name)
        dc_node_copy=dc_node_copy.replace("[POW_difficulty]", str(node["POW_difficulty"]))
        if node["tip_selection"]:
            dc_node_copy=dc_node_copy.replace("[cache]", "goshimmer-cache")
        else:
            dc_node_copy=dc_node_copy.replace("[cache]", "goshimmer_without_tipselection-cache")
        dc_node_copy=dc_node_copy.replace("[port_API]", str(port_API))
        dc_node_copy=dc_node_copy.replace("[port_dashboard]", str(port_dashboard))
        dc_node_copy=dc_node_copy.replace("[port_prometheus]", str(port_prometheus))
        dc_node_copy=dc_node_copy.replace("[CPUS]", str(node["cpus"]))
        dc_node_copy=dc_node_copy.replace("[CPUSET]", str(cpuset_num) + "," + str(cpuset_num + 1))

        # TODO: limit to the actual number of CPU cores available
        cpuset_num += 2

        dc_file.write(dc_node_copy)

        port_dict[node_name]=dict()
        port_dict[node_name]["API"]=port_API
        port_dict[node_name]["dashboard"]=port_dashboard
        port_dict[node_name]["prometheus"]=port_prometheus

        port_API+=100
        port_dashboard+=100
        port_prometheus+=1
    return port_dict

def write_dc_end(dc_file, dc_end, list_of_nodes):
    str_list_of_nodes=list()
    for node_name in list_of_nodes:
        str_list_of_nodes.append("    - "+node_name)
    dc_file.write(dc_end.replace(
        "[list_of_nodes]", "\n".join(str_list_of_nodes)
    ))

if __name__=="__main__":
    create_docker_compose_file()
