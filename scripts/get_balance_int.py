#!/usr/bin/env python3

import os

BALANCE_COMMAND="./cli-wallet_Linux_x86_64 balance"

stream = os.popen(BALANCE_COMMAND)
output = stream.read()
if "BALANCE" not in output:
    print("N/A")
elif "<EMPTY>" in output:
    print("0")
else :
    """
    output =

    IOTA Pollen CLI-Wallet 0.1

    STATUS	BALANCE			COLOR						TOKEN NAME
    ------	---------------		--------------------------------------------	-------------------------
    [ OK ]	1315			IOTA						IOTA

    """
    output=output.split("]")[1]
    output=output.split("IOTA")[0]
    output=int(output) # Delete spaces and tabulations
    print(str(output))
