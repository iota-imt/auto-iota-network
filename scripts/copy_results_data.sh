#!/bin/sh
NOW=$(date +%Y-%m-%d-%T)


SELF=`readlink -f "$0"`
SELFDIR=`dirname "$SELF"`
ORIGBASEDIR="$SELFDIR"/../
OUTPUTDIR="$ORIGBASEDIR"/extract_data/"$NOW"

mkdir "$OUTPUTDIR"

echo writing to "$OUTPUTDIR"

cd "$ORIGBASEDIR"

files="list_of_nodes.json \
    extract_data/prometheus.csv
    wallets/*/*.csv"
 
for val in ${files}; do
    mkdir -p "$OUTPUTDIR"/"$(dirname $val)"
    cp $val "$OUTPUTDIR"/"$(dirname $val)/"
done

