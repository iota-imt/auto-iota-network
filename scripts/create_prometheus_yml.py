#!/usr/bin/env python3

# PRO = prometheus

import json

PRO_BLANK_FILE = "prometheus_yml/BLANK_prometheus.yml"
PORT_DICT_FILE = "port_dict.json"
OUTPUT_FILE_PATH = "prometheus.yml"

def create_prometheus_yml(pro_blank_file=PRO_BLANK_FILE,
        port_dict_file=PORT_DICT_FILE, output_file_path=OUTPUT_FILE_PATH):
    with open(port_dict_file, 'r') as f : nodes=json.load(f)
    with open(pro_blank_file, 'r') as f : str_pro_blank_file = f.read()
    with open(output_file_path, 'w') as output_file :
        output_file.write(str_pro_blank_file)
        for node_name in nodes :
            output_file.write(
                "\n        - " +
                node_name + ":" +
                str(nodes[node_name]["prometheus"])
            )

if __name__=="__main__":
    create_prometheus_yml()
