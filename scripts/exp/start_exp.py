#!/usr/bin/env python3
import requests
import json
import os
import subprocess
import time
import extract_prometheus_csv
import shutil
import getpass

def get_parameters():
    with open("parameters_of_exps.json") as f:
        param_of_exps = json.load(f)
        print("Here are the parameters of different experiences:")
        print (param_of_exps)
    return param_of_exps

# create list_of_node.json
#def create_nodes_cfg(nb_nodes, PoW, tps):
def create_nodes_cfg(exp_cfgs):
    nodes_cfg = {}
    node_id = 0
    for cfg in exp_cfgs.keys():
        cfg_value = exp_cfgs.get(cfg)
        nb_nodes = cfg_value["nb_of_nodes"]
        tps = cfg_value["tps"]
        PoW = cfg_value["pow"]
         # Create our own json centent in form of dict
        for i in range(int(nb_nodes)):
            node_id += 1
            nodes_cfg['node'+str(node_id)] = {'transactions_per_second':int(tps), 'POW_difficulty':int(PoW)}
    print(nodes_cfg)
    # Write dict into json file
    with open("../../list_of_nodes.json", "w") as f:
        json.dump(nodes_cfg, f,indent = 1)
        f.close()
        print("Has written list_of_nodes.json which has content below:")
        os.system("cat ../../list_of_nodes.json")

# start the experimant
# return 0 if the experimant has started successfully
def start(mpms):
    print("\nstarting the exp")
    os.chdir("../../")
    r = os.popen('date -u "+%Y-%m-%dT%H:%M:%S"')
    start_time = r.readlines()[0].strip()
    nb_nodes = 0
    for mpm in mpms:
        nb_nodes += mpm[0]
    cmd = "./run.sh %d %d %d"%(len(mpms), mpms[0][0], mpms[0][1])
    if (len(mpms) > 1):
        cmd = "./run.sh %d %d %d %d %d"%(len(mpms), mpms[0][0], mpms[0][1], mpms[1][0], mpms[1][1])
    print("cmd to start run.sh: %s"%cmd)
    return start_time, os.system(cmd)

# stop the experimant
def stop():
    print("stopping the exp")
    return os.system("./down.sh")

# get mpms
# return example: [[2,500],[4,600]] means cfg_1: 2 nodes of 500 mpm, 4 nodes of 600 mpm
def get_mpms(exp_cfgs):
    ans = []
    i = 0
    for cfg_value in exp_cfgs.values():
        tmp = [cfg_value["nb_of_nodes"], cfg_value["mpm"]]
        ans.append(tmp)
    return ans


#def start_one_exp(exp, nb_nodes, PoW, tps, mpm, duration):
def start_one_exp(exp, params_of_exp):
    print("Going to write list_of_nodes.json")
    print (params_of_exp["exp_cfgs"])
    exp_cfgs = params_of_exp["exp_cfgs"]

    create_nodes_cfg(exp_cfgs)

    mpms = get_mpms(exp_cfgs)
    start_time, start_res = start(mpms)
    duration = params_of_exp["duration"]
    print("started the exp, waiting for %s mins..."%(duration))
    time.sleep(60 * float(duration))
    r = os.popen('date -u "+%Y-%m-%dT%H:%M:%S"')
    end_time = r.readlines()[0].strip()


    file_name = "%s.csv"%exp
    export_data(start_time, end_time, file_name)

    end_res = stop()
    os.chdir("scripts/exp")
    print("The exp %s has finished and been stoped"%exp)

def export_data(start_time, end_time, file_name):
    print("Exporting prometheus data...")
    extract_prometheus_csv.main(start_time, end_time, file_name)
    #os.system("python3 extract_prometheus_csv.py %s %s %s"%(start_time, end_time, file_name))
    print("finished exporting data")

# Copy trx_duration.csv file of each node into data/transaction director
def get_trx_duration_file():
    user_name = getpass.getuser()
    dst_dir = os.path.abspath("/home/%s/stage-ismael-collet/auto_iota_network/scripts/exp/data/transaction"%user_name)
    src_dir = os.path.abspath("/home/%s/stage-ismael-collet/auto_iota_network/wallets"%user_name)

    file_name = "trx_duration"
    if not os.path.exists(dst_dir):
        os.makedirs(dst_dir)

    if os.path.exists(src_dir):
        for root, dirs, files in os.walk(src_dir):
            for node_dir in dirs:
                trx_file = os.path.join(os.path.join(root, node_dir), file_name + ".csv")
                print (trx_file)
                shutil.copy(trx_file, os.path.join(dst_dir, file_name + "_%s.csv"%node_dir))


    else:
        print("wallets directory doesn't exist")



def main():
    print(getpass.getuser())
    params = get_parameters()
    for exp in params.keys():
        print("***********************Experiment %s **************************"%(exp))
        params_of_exp = params.get(exp)
        duration = params_of_exp["duration"]
        cfgs = params_of_exp["exp_cfgs"]
        for cfg in cfgs.keys():
            cfg_value = cfgs.get(cfg)
            print(cfg_value)
            print("number of nodes: %d"%cfg_value['nb_of_nodes'])
            print("            pow: %d"%cfg_value['pow'])
            print("            tps: %d"%cfg_value['tps'])
            print("            mpm: %d"%cfg_value['mpm'])
            print("  duration(min): %d"%duration)
            print("--------------------------------------------------------------")
        start_one_exp(exp, params_of_exp)


main()
