# Automation Script User Guide

In this document, you will learn how to:
- access the server remotely
- set up experiments
- start experiments
- download csv files generated from the server
- analyse the data

The goal is to run experiments using the Spammer plugin in a automatic way.

## Access the server
You will need first to set up your vpn (we used openvpn) with a given configuration file:

`sudo openvpn --config <your vpn file, for exemple: enstb.conf>`

Once the vpn is working, you can access the tangle server with:

`ssh -4 <usr_name>@tangle-01.enstb.org`

For both command, you will need specific passphrases.

## Setting up experiments 
To set up experiments, we will write all the parameters of the experiments in a 
json file  [parameters_of_exps.json](https://gitlab.imt-atlantique.fr/iota-imt/stage-ismael-collet/-/blob/cb8571d6d6abd34f845c45d2ff07b93f70b9aa2b/auto_iota_network/scripts/exp/parameters_of_exps.json). 
For each experiment, you can specify these parameters: 

- Duration of experiment (in minutes)
- And we can define up to 2 configurations inside of this experiment which permits us to run asymmetric network. For each configuration, we specify these parameters (we only need to define one configuration if the network is symmetrical):
  - Number of nodes in the network
  - Proof of work (PoW) - that a node needs to perform to send messages
  - Messages per minute (mpm) - sent by the node
  - Transactions per second (tps) - sent by the node

Every node inside one experiment share the same PoW and tps value. Here's an example:

```json
{
	"exp_a":{
		"duration": 5,
		"exp_cfgs":{
			"cfg_1":{
				"nb_of_nodes":	2,
				"pow":	5,
				"mpm":	300,
				"tps":	1
			},
			"cfg_2":{
				"nb_of_nodes":	2,
				"pow":	5,
				"mpm":	100,
				"tps":	1
			},
    }	
	},

	"exp_b":{
		"duration": 5,
		"exp_cfgs":{
			"cfg_1":{
				"nb_of_nodes":	2,
				"pow":	5,
				"mpm":	300,
				"tps":	1
			}
    }	
	}
}

```

In this example, we defined two experiments: `exp_a` and `exp_b`.

 `exp_a` is an experiment with asymmetrical network which will run for 5 minutes and contains two configurations of node `cfg_1` and `cfg_2`.  `exp_a` has 4 nodes, 2 of `cfg_1` and 2 of `cfg_2`. Each of them have a PoW of 5,  a tps of 1, and a different mpm. The first two nodes whose dashboard port are 8081and 8181 belong to `cfg_1`  and have the mpm of 300. The following two nodes whose dashboard port are 8281 and 8381 have the configuration of `cfg_2` and mpm of 100. 

`exp_b` is an experiment with a symmetrical network which will run for 5 unites too but contains two nodes with same PoW value, tps and mpm. 

In this way, we can define symmetrical and asymmetric network in one json file.

<!--Maybe the following needs to be changed?-->
To do so, you can follow theses steps:   
1. `cd stage-ismael-collet/auto_iota_network/scripts/exp` 
 Change the current working directory. In this directory you will find `parameters_of_exps.json`.
 You can open it to specify the parameters:
2. `vim parameters_of_exps.json` or `nano parameters_of_exps.json`
Make the change, save it and close the file.

2bis. If you wish to analyse the data later, GENERATE the json file through the jupyter notebook `define_merge_parameters.ipynb`.
0. To download the file:
`scp -r <your_user_name>@tangle-01.enstb.org:/home/<your_user_name>/stage-ismael-collet/auto_iota_network/scripts/exp/analysis/define_merge_parameters.ipynb <local_directory>`
1. Start Jupyer on you local machine: `jupyter notebook` or via Anaconda
2. Launch the notebook from where you copied it and follow the instructions


## Start the script

After defining the experiments you want to run, it's time to give it a go! 
The script `start_exp.py` is in the same directory as `parameters_of_exps.json` and 
 all of the prometheus data related to the experiments which will be analysed 
 is also saved here in `data` sub-directory.
You can just launch it:

1. `python3 start_exp.py`

   Here's what it does. 
- Firstly, it gets all of the information related to the experiments needed 
   from  [parameters_of_exps.json](https://gitlab.imt-atlantique.fr/iota-imt/stage-ismael-collet/-/blob/cb8571d6d6abd34f845c45d2ff07b93f70b9aa2b/auto_iota_network/scripts/exp/parameters_of_exps.json). 
- Then for each exp, it will generate a list_of_node.json, 
start the network by launching `run.sh`, 
wait for the given time, 
export the data to the repository [data](https://gitlab.imt-atlantique.fr/iota-imt/stage-ismael-collet/-/tree/auto_exp/auto_iota_network/scripts/exp/data), 
and finally stop the network. 

This is for one experiment, it goes through the same routine for each experiment. 
So after the script `start_exp.py` terminates, 
there will be a csv file which contains all of the data extracted from prometheus for each experiment. 
The name of the csv file is the name of its corresponding experiment defined in  [parameters_of_exps.json](https://gitlab.imt-atlantique.fr/iota-imt/stage-ismael-collet/-/blob/cb8571d6d6abd34f845c45d2ff07b93f70b9aa2b/auto_iota_network/scripts/exp/parameters_of_exps.json).

## Dashboard

You can go to `http://tangle-01.enstb.org:8081/dashboard` to monitor what is 
going on in the network.

## Download the data

At this point, the only thing left to do is to download the csv files generated on server to your machine for your analyse. It's simple:
1. Open a terminal on you local machine
2. If you didn't concatenate the files:
 `scp -r <your_user_name>@tangle-01.enstb.org:/home/<your_user_name>/stage-ismael-collet/auto_iota_network/scripts/exp/data <local_directory>`

This command enables you to download the whole data directory to your local directory. 
And now you have the data you need to analyse.
Once you copied the data, try to think about cleaning it for the next set of experiments 
(it is not necessary but you end up copying a lot of files otherwise).

## Merge the parameters with the data
Follow the steps in the jupyer notebook.

## Analyse the data

To download the file:
`scp -r <your_user_name>@tangle-01.enstb.org:/home/<your_user_name>/stage-ismael-collet/auto_iota_network/scripts/exp/analysis/analysis.ipynb <local_directory>`

To analyse the experiment data, we used Jupyter Notebook (with Anaconda) on our local machines.
(1. Start Jupyer on you local machine: `jupyter notebook` or via Anaconda)
2. Launch the notebook from where you copied it and follow the instructions
