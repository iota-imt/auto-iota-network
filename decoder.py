import csv  # Used to write to the decoded one
import sys  # Used to fetch the first command line argument, which represents the file to decode

def file_to_list(filename):
    """
    Turns a file with raw data into a list containing a line per message
    """
    with open(filename) as file:
        data = file.read()
        data = data.replace(' ', '')
        pos = len("solidification_time;message_id;payload")  # Pos represents a cursor which will move through the file
        open_brackets = 0
        data_list = []
        while pos < len(data):
            try :
                pos += 1
                solidification_time, id, pos = find_start_bracket(data, pos)
                pos += 1
                open_brackets += 1
                temp_str = "{\nsolidificationTime:" + str(solidification_time)
                while open_brackets != 0:  # This block prevents us from breaking a message into different pieces
                    if data[pos] == '{':
                        open_brackets += 1
                    if data[pos] == '}':
                        open_brackets -= 1
                    temp_str += data[pos]
                    pos += 1
                data_list.append(temp_str)
            except:
                return data_list
        return data_list

def find_start_bracket(text, init_pos):
    """
    Analyses a line by retrieving the position of the first bracket and the two int numbers before that (the format is %d;%d;{message})
    :param text: The text to analyse
    :param init_pos: The position we are currently at (so where we start searching from)
    """
    pos = init_pos
    first_int = None
    second_int = None
    while text[pos] != '{':
        if text[pos] == ';':
            if first_int == None:
                first_int = text[init_pos:pos]
            else:
                second_int = text[init_pos:pos]
            init_pos = pos + 1
        pos += 1
    return first_int, second_int, pos

def analyse_data(data):
    """
    Simple function to call analyse_entry to all elements in the list data
    """
    for i in range(len(data)):
        data[i] = analyse_entry(data[i])
    return data

def analyse_entry(data):
    """
    Dumps all keys that are enclosed in brackets, such as the elements of the payload
    """
    pos = 2
    key = ""
    value = ""
    dic = {}
    brackets = 0
    listing = 0
    while pos < len(data) - 1:
        try :
            while not ord(data[pos]) in range(ord('A'), ord('z')):
                pos += 1
            while data[pos] != ":":
                key += data[pos]
                pos += 1
            pos += 1
            while data[pos] != '\n' or listing != 0:
                if data[pos] == '{':
                    brackets += 1
                if data[pos] == '[':
                    listing += 1
                if data[pos] == '}':
                    brackets -= 1
                if data[pos] == ']':
                    listing -= 1
                value += data[pos]
                pos += 1
            if brackets != 0:
                key = 'payload-' + str(key)
            if '{' in value:
                value = value[:-1]  # Remove the '{' from the value which represents the payload type
            dic[key] = value
            key = ""
            value = ""
        except:
            return dic
    return dic

def merge_as_csv(data, filename):
    """
    Merges the data list in a single csv file, each key found at least once will be present for all messages displaying it
    """
    keys = []
    for i in data:
        for key in i:
            if key not in keys:
                keys.append(key)
            i[key] = i[key].replace('\n', '')
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        row = []
        for key in keys:
            row.append(str(key))
        writer.writerow(row)
        for i in data:
            row = []
            for key in keys:
                if key in i:
                    row.append(i[key])
                else:
                    row.append('')
            writer.writerow(row)

def main():
    filename = sys.argv[1]  # We fetch the command line argument
    data = file_to_list(filename)  # The fetch data from the file
    an = analyse_data(data)  # Analyse it to break it into key/values pairs
    merge_as_csv(an, filename[:-4]+"-decoded.csv")  # And finally write it back into the decoded file


if __name__ == "__main__":
    main()
