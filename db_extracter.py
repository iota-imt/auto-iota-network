import json  # Used to read the list of nodes file and generate the nodes
from os import system  # Allows interraction with the command line utilities such as docker cp or mkdir
from datetime import datetime  # Allows easy access to a human readable time format used to separate experiments

with open("list_of_nodes.json", 'r') as file:  # Read the list of nodes file
    list_of_nodes = json.loads(file.read())

nodes = []

for key in list_of_nodes:
    nodes.append(key+'_1')

print(nodes)  # Displays the nodes

curr_time = str(datetime.now())[:19].replace(' ', '-') # Time we stopped the experience at
curr_time = curr_time.replace(':', '-')  # The ':' would confuse docker

for node in nodes:
    command = f"mkdir -p data/{curr_time}/{node}/"  # Create the directory where we store the results (and data if needed thanks to the -p flag)
    system("echo '" + command + "'")
    system(command)
    command = f"docker cp {node}:/tmp/data.csv data/{curr_time}/{node}/"  # Copy the files from the container to the server
    system("echo '" + command + "'")
    system(command)
    command = f"python3 decoder.py data/{curr_time}/{node}/data.csv"  # Launch the decoding program to make the data more usable
    system("echo '" + command + "'")
    system(command)
